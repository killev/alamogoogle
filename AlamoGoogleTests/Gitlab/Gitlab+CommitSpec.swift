//
//  Gitlab+CommitSpec.swift
//  AlamoGoogleTests
//
//  Created by Peter Ovchinnikov on 8/27/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import AlamoGoogle

import Quick
import Nimble

class GitlabCommitSpec: QuickSpec {
    override func spec() {
        describe("Gitlab") {
            describe("Commits") {
                let api = Gitlab.api()
                var project: ResponseSignal<Project>!

                beforeEach {
                    project = api.project(id: "13776067")
                }
                it("should return list of commits for projects") {

                    let commits = project
                        .flatMapLatest { $0.commits() }

                    waitUntil(timeout: 20) { done in
                        _ = commits.observeNext { commits in
                            expect(commits.count).to(beGreaterThan(1))
                            done()
                        }
                        _ = commits.observeFailed { error in
                            fail("Faced error: \(error.localizedDescription)")
                            done()
                        }
                    }
                }
            }
        }
    }
}
