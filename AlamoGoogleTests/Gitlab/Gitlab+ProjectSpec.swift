//
//  Gitlab+ProjectSpec.swift
//  AlamoGoogleTests
//
//  Created by Peter Ovchinnikov on 8/21/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import AlamoGoogle

import Quick
import Nimble

class GitlabProjectSpec: QuickSpec {
    override func spec() {
        describe("Gitlab") {
            let api = Gitlab.api()
            describe("Project") {
                it("should return list of public projects") {
                    let projects = api.projects()
                    waitUntil(timeout: 20) { done in
                        _ = projects.observeNext { projects in
                            expect(projects.count).to(beGreaterThan(1))
                            done()
                        }
                        _ = projects.observeFailed { error in
                            fail("Faced error: \(error.localizedDescription)")

                            done()
                        }
                    }
                }

                it("should return single public project for given id") {

                    let project = api.project(id: "13776067")
                    waitUntil(timeout: 20) { done in
                        _ = project.observeNext { project in
                            expect(project.id).to(equal("13776067"))
                            done()
                        }
                        _ = project.observeFailed { error in
                            print(error.domain)
                            print(error.code)

                            fail("Faced error: \(error.localizedDescription)")
                            done()
                        }
                    }
                }
            }
        }
    }
}
