//
//  Alamofire+SignalSpec.swift
//  AlamoGoogleTests
//
//  Created by Peter Ovchinnikov on 8/19/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

// swiftlint:disable function_body_length

import AlamoGoogle
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import ReactiveKit

import Quick
import Nimble

struct SimpleItem {
    let conditions: String //"Partly cloudy"
    //TODO: Replace to enum
    let day: String //"Monday"
    let temperature: Float // 20
}

struct SimpleObject {
    let location: String //"Partly cloudy"
    let forecast: [SimpleItem]
}

extension SimpleItem: ImmutableMappable {
    init(map: Map) throws {
        conditions = (try? map.value("conditions")) ?? ""
        day = (try? map.value("day")) ?? ""
        temperature = (try? map.value("temperature")) ?? 0
    }
}

extension SimpleObject: ImmutableMappable {
    init(map: Map) throws {
        location = (try? map.value("location")) ?? ""
        forecast = (try? map.value("three_day_forecast")) ?? []
    }
}

class AlamofireResponseArraySpec: QuickSpec {

    override func spec() {

        describe("Alamofire") {
            describe("responseArray") {
                //swiftlint:disable:next line_length
                let url = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/master/sample_array_json"

                //swiftlint:disable:next line_length
                let mistakenUrl = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/master/sample_array_json1"
                var bag: DisposeBag!
                beforeEach {
                    bag = DisposeBag()
                }

                it("veryfy flatMapLatestChain") {

                    let signal = SafeSignal(sequence: [1, 2], interval: 5).eraseType()

                    let result = signal.flatMapLatest {
                        return AF
                            .request(url,
                                     method: .get,
                                     parameters: nil,
                                     encoding: URLEncoding.default,
                                     headers: [:])
                            .responseArray(SimpleItem.self, keyPath: "")
                    }
                    var count = 0

                    result.observeNext { _ in
                        count += 1
                    }.dispose(in: bag)

                    expect(count).toEventually(be(2), timeout: 60)
                }

                it("Should retrive and map JSON to array of Immutable items") {

                    let result = AF
                        .request(url,
                                 method: .get,
                                 parameters: nil,
                                 encoding: URLEncoding.default,
                                 headers: [:])
                        .responseArray(SimpleItem.self, keyPath: "")

                    waitUntil(timeout: 20) { done in
                        result.observeNext { items in
                            expect(items.count).to(be(3))
                            done()
                            }.dispose(in: bag)
                    }
                }

                it("Should handle alamofire error and turn them into Failure Stream") {

                    let result = AF
                        .request(mistakenUrl,
                                 method: .get,
                                 parameters: nil,
                                 encoding: URLEncoding.default,
                                 headers: [:])
                        .responseArray(SimpleItem.self, keyPath: "")

                    waitUntil(timeout: 20) { done in
                        result.observeFailed { _ in
                            done()
                            }.dispose(in: bag)
                    }
                }

            }
            describe("responseObject") {
                var bag: DisposeBag!
                beforeEach {
                    bag = DisposeBag()
                }

                let url = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/master/sample_json"

                //swiftlint:disable:next line_length
                let mistakenUrl = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/master/sample_json1"

                it("Should retrive and map JSON to immutable object") {

                    let result = AF
                        .request(url,
                                 method: .get,
                                 parameters: nil,
                                 encoding: URLEncoding.default,
                                 headers: [:])
                        .responseObject(SimpleObject.self, keyPath: "")

                    waitUntil(timeout: 20) { done in
                        result.observeNext { object in
                            expect(object.location).to(equal("Toronto, Canada"))//GOT from json
                            expect(object.forecast.count).to(be(3))
                            done()
                            }.dispose(in: bag)
                    }
                }

                it("Should handle alamofire error and turn them into Failure Stream") {

                    let result = AF
                        .request(mistakenUrl,
                                 method: .get,
                                 parameters: nil,
                                 encoding: URLEncoding.default,
                                 headers: [:])
                        .responseObject(SimpleObject.self, keyPath: "")

                    waitUntil(timeout: 20) { done in
                        result.observeFailed { _ in
                            done()
                        }.dispose(in: bag)
                    }
                }

            }
        }
    }
}
