//
//  Project+Operations.swift
//  AlamoGoogle-iOS
//
//  Created by Peter Ovchinnikov on 8/27/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Alamofire

public enum ProjectParameters: String {
    case id123331
}

public extension GitlabAPI {
    func projects(_ parameters: [ProjectParameters: Any] = [:])->ResponseSignal<[Project]> {

        let url = baseUrl
            .appendingPathComponent(GitlabPath.projects)

        return AF
            .request(url,
                     method: .get,
                     parameters: nil,
                     encoding: URLEncoding.default,
                     headers: [:])
            .responseArray(Project.self, keyPath: "", context: self)

    }
    func project(id: String)->ResponseSignal<Project> {

        let url = baseUrl
            .appendingPathComponent(GitlabPath.projects)
            .appendingPathComponent(id)

        return AF
            .request(url,
                     method: .get,
                     parameters: nil,
                     encoding: URLEncoding.default,
                     headers: [:])
            .responseObject(Project.self, keyPath: "", context: self)

    }
}
