//
//  Gitlab.swift
//  AlamoGoogle-iOS
//
//  Created by Peter Ovchinnikov on 8/21/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReactiveKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

struct GitlabPath {
    static let projects = "projects"
    static let commits = "repository/commits"
    //static let projects = "projects"
}

public struct Gitlab {
    private init() {}
    public static func api(token: String = "") -> GitlabAPI {
        return GitlabAPI(token: token)
    }
}

public struct GitlabAPI {
    let token: String
    let baseUrl = URL(string: "https://gitlab.com/api/v4/")!
}

extension GitlabAPI: MapContext {

}
