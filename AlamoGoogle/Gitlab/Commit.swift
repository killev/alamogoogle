//
//  Commit.swift
//  AlamoGoogle-iOS
//
//  Created by Peter Ovchinnikov on 8/21/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ObjectMapper

public struct Commit {
    let api: GitlabAPI
    public let id: String
}

extension Commit: ImmutableMappable {
    public init(map: Map) throws {
        api = map.context as! GitlabAPI // swiftlint:disable:this force_cast
        id = (try? map.value("id")) ?? ""
    }
}
