//
//  Commit+Operations.swift
//  AlamoGoogle-iOS
//
//  Created by Peter Ovchinnikov on 8/27/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Alamofire

public enum CommitParameters: String {
    case id
}

public extension Project {
    func commits(_ parameters: [CommitParameters: Any] = [:]) ->ResponseSignal<[Commit]> {
        let url = api.baseUrl
            .appendingPathComponent(GitlabPath.projects)
            .appendingPathComponent(toString(id))
            .appendingPathComponent(toString(GitlabPath.commits))

        return AF
            .request(url,
                     method: .get,
                     parameters: nil,
                     encoding: URLEncoding.default,
                     headers: [:])
            .responseArray(Commit.self, keyPath: "", context: self.api)
    }
}
