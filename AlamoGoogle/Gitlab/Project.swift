//
//  Project.swift
//  AlamoGoogle-iOS
//
//  Created by Peter Ovchinnikov on 8/21/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ObjectMapper
import Alamofire

public struct Project {
    let api: GitlabAPI

    public let id: String
    public let projectDescription: String
    public let name: String
    public let nameWithNamespace: String
    public let path: String
    public let pathWithNamespace: String
    public let createdAt: Date
    public let defaultBranch: String
    public let tagList: [String]
    public let sshURLToRepo: String
    public let httpURLToRepo: String
    public let webURL: String
    public let readmeURL: String
    public let avatarURL: String
    public let starCount: Int
    public let forksCount: Int
    public let lastActivityAt: Date
    public let namespace: Namespace
}

// MARK: - Namespace
public struct Namespace {

    public let id: String
    public let name: String
    public let path: String
    public let kind: String
    public let fullPath: String
    public let parentID: String
    public let avatarURL: String
    public let webURL: String
}

extension Namespace: ImmutableMappable {
    public init(map: Map) throws {
        id = "\(try (map.value("id") as Int))"
        name = (try? map.value("name")) ?? ""
        path = (try? map.value("path")) ?? ""
        kind = (try? map.value("kind")) ?? ""
        fullPath = (try? map.value("full_path")) ?? ""
        parentID = (try? map.value("parent_id")) ?? ""
        avatarURL = (try? map.value("avatar_url")) ?? ""
        webURL = (try? map.value("web_url")) ?? ""

    }
}

extension Project: ImmutableMappable {
    public init(map: Map) throws {
        api = map.context as! GitlabAPI // swiftlint:disable:this force_cast
        id = "\(try (map.value("id") as Int))"

        projectDescription = (try? map.value("description")) ?? ""
        name  = (try? map.value("name")) ?? ""
        nameWithNamespace = (try? map.value("name_with_namespace")) ?? ""
        path = (try? map.value("path")) ?? ""
        pathWithNamespace = (try? map.value("path_with_namespace")) ?? ""
        createdAt = (try? map.value("created_at")) ?? Date()
        defaultBranch = (try? map.value("default_branch")) ?? ""
        tagList = (try? map.value("tag_list")) ?? []
        sshURLToRepo = (try? map.value("ssh_url_to_repo")) ?? ""
        httpURLToRepo = (try? map.value("http_url_to_repo")) ?? ""
        webURL = (try? map.value("web_url")) ?? ""
        readmeURL = (try? map.value("readme_url")) ?? ""
        avatarURL = (try? map.value("avatar_url")) ?? ""
        starCount = (try? map.value("star_count")) ?? 0
        forksCount = (try? map.value("forks_count")) ?? 0
        lastActivityAt = (try? map.value("last_activity_at")) ?? Date()
        namespace = try map.value("namespace")
    }
}
