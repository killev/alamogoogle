//
//  Alamofire+Signal.swift
//  AlamoGoogle-iOS
//
//  Created by Peter Ovchinnikov on 8/19/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Foundation
import ReactiveKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

public typealias ResponseSignal<T> = Signal<T, NSError>

public extension DataRequest {

    func responseObject<T: ImmutableMappable>(_ type: T.Type,
                                              queue: DispatchQueue = .main,
                                              keyPath: String? = nil,
                                              context: MapContext? = nil) -> ResponseSignal<T> {

        return ResponseSignal { observer in
            self.responseObject(queue: queue,
                                keyPath: keyPath,
                                context: context) { (response: DataResponse<T>) -> Void in

                //DataRequest.printResponse(response: response)
                switch response.result {

                case .success(let result):
                    observer.receive(lastElement: result)
                case .failure(let error):
                    observer.receive(completion: .failure(error as NSError))

                }
            }
            return NonDisposable.instance
        }
    }

   func responseArray<T: ImmutableMappable>(_ type: T.Type,
                                            queue: DispatchQueue = .main,
                                            keyPath: String? = nil,
                                            context: MapContext? = nil) -> ResponseSignal<[T]> {

        return ResponseSignal { observer in
            self.responseArray(queue: queue,
                               keyPath: keyPath,
                               context: context) { (response: DataResponse<[T]>) -> Void in

                //DataRequest.printResponse(response: response)

                switch response.result {

                case .success(let result):
                    observer.receive(lastElement: result)
                case .failure(let error):
                    observer.receive(completion: .failure(error as NSError))

                }
            }
            return NonDisposable.instance
        }
    }
}
