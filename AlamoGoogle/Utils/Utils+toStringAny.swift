//
//  Utils+toStringAny.swift
//  AlamoGoogleTests
//
//  Created by Peter Ovchinnikov on 8/27/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

func toString(_ val: Any?, def: String = "") -> String {
    if let val = val {
        return "\(val)"
    }
    return def
}
